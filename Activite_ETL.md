# Activité ETL

L'objectif est de réviser la construction d'un pipeline ETL en vue d'exploiter
des données analytiques :

1. Ingestion des données
2. Transformation des données
3. Chargement des données

## Objectif

Pour une recherche d'offre donnée, nous souhaitons pouvoir visualiser
la répartition des offres d'emploi par département.

Une représentation visuelle sous forme de choroplèthe semble bien adapté
à cette problématique. Les métadonnées des départements au format GeoJSON
sont disponibles sur [GitHub](https://github.com/gregoiredavid/france-geojson/blob/master/departements.geojson).

Il semble que Metabase permette assez simplement de pouvoir utiliser de la
[cartographie personnalisée](https://www.metabase.com/docs/latest/administration-guide/20-custom-maps.html)
au format GeoJSON.

## Jeu de données

Nous allons (ré-)utiliser les données d'offres d'emploi depuis le site Indeed,
via l'utilisation de leurs flux RSS. Pour récupérer les offres d'un métier,
il suffit de requêter une URL de la forme :

```
https://fr.indeed.com/rss?q=data+analyst&l=France&fromage=last&sort=date
```

Pour chaque il est possible d'obtenir les données suivantes :

- Date de publication
- Référence unique
- Titre
- Ville
- Entreprise

Pour télécharger et parser un fichier RSS, vous pouvez utiliser the paquet
Python [`feedparser`](https://pypi.org/project/feedparser/).

## Pipeline

Voici une suggestion d'étapes pour effectuer le pipeline :

1. Récupérer et stocker des offres d'emploi depuis Indeed au format CSV (_Python_)
2. Charger les données brutes CSV dans une base SQLite (_Bash_)
3. Transformer les données (_SQL_)
4. Charger les données propres dans un modèle de données analytique en étoile (_SQL_)
5. Enrichir les données de localisation avec un géocodage, permettant d'obtenir
   la région, le département et les coordonnées (_Python_)

Quelques conseils pour le géocodage :

- Vous pouvez utiliser le paquet Python [`geopy`](https://pypi.org/project/geopy/)
- Vous pouvez utiliser le géocodeur "Nominatim" en respectant les limites de requêtes (1 par seconde)
- L'utilisation d'un schéma en étoile vous permettra de ne géocoder qu'une seule fois chaque localisation
  et ainsi gagner du temps !

## Modèle de données

À titre d'exemple, un schéma de modèle de données analytique en étoile :

![Modele Donnees](images/revision-star-schema.png)
